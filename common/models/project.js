module.exports = function (Project) {
	// Project.belongsTo(User, {foreignKey: "ownerId"}); // Alternative Relation Model

	// list project
	Project.listProjects = function (callback) {
		Project.find({
			fields: {
				balance: false
			}
		}, callback);
	}
	Project.remoteMethod("listProjects", {
		returns: { arg: "projects", type: "array" },
		http: { path: "/list-projects", verb: "get" }
	});

	// donate
	Project.donate = function (id, amount, callback) {
		Project.findById(id, function(err, project) {
			if (err) return callback(err);

			project.balance += amount;
			project.save();

			callback(null, true);
		});
	}
	Project.remoteMethod("donate", {
		accepts: [
			{ arg: "id", type: "number" },
			{ arg: "amount", type: "number" },
		],
		returns: { arg: "success", type: "boolean" },
		http: { path: "/donate", verb: "post" }
	});

	// withdraw
	Project.withdraw = function (id, amount, callback) {
		Project.findById(id, function(err, project) {
			if (err) return callback(err);

			project.balance = (project.balance >= amount) ? (project.balance - amount) : 0;
			project.save();

			callback(null, true);
		});
	}
	Project.remoteMethod("withdraw", {
		accepts: [
			{ arg: "id", type: "number" },
			{ arg: "amount", type: "number" }
		],
		returns: { arg: "success", type: "boolean" },
		http: { path: "/withdraw", verb: "post" }
	});
};
