module.exports = function(app) {
	var User = app.models.user;
	var Role = app.models.Role;
	var RoleMapping = app.models.RoleMapping;
	var Team = app.models.Team;

	User.create([
		{ username: "John", email: "john@doe.com", password: "opensesame" },
		{ username: "Celine", email: "celine@tan.com", password: "celinetan" },
		{ username: "admin", email: "go@admin.com", password: "admin123" },
	], function (err, users) {
		if (err) throw err;

		console.log("Created Users : ", users);

		// Create project 1 and make john the owner
		users[0].projects.create({
			name: "Project 1",
			balance: 100
		}, function(err, project) {
			if (err) throw err;

			console.log("Created project : ", project);

			// Add Team Members
			Team.create([
				{ ownerId: project.ownerId, memberId: users[0].id },
				{ ownerId: project.ownerId, memberId: users[1].id },
			], function (err, team) {
				if (err) throw err;

				console.log("Created Team : ", team);
			});
		});

		// Create project 2 and make Celine the owner
		users[1].projects.create({
			name: "Project 2",
			balance: 100
		}, function(err, project) {
			if (err) throw err;

			console.log("Created project : ", project);

			Team.create({
				ownerId: project.ownerId,
				memberId: users[1].id
			}, function(err, team) {
				if (err) throw err;

				console.log("Created Team : ", team);
			});
		});

		// Create Admin Role
		Role.create({
			name: "admin"
		}, function(err, role) {
			if (err) throw err;

			console.log("Created Role : ", role);

			// make admin an Admin Role
			role.principals.create({
				principalType: RoleMapping.USER,
				principalId: users[2].id
			}, function(err, principal) {
				if (err) throw err;

				console.log("Created Principal : ", principal);
			});
		});
	});
}